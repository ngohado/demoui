package app.vn.demoapp.fragments;

import android.os.Bundle;
import android.view.View;

import app.vn.demoapp.R;

/**
 * Created by Thanh Le on 13/03/2017.
 */

public class MainFragment extends BaseFragment {

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected void getArgument(Bundle bundle) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    protected void initView(View root) {

    }
}
