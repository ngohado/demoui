package app.vn.demoapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Thanh Le on 13/03/2017.
 */

public abstract class BaseFragment extends Fragment{
    protected View rootView;

    private Unbinder mUnbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(getLayoutId(), container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnbinder = ButterKnife.bind(this, view);
        if (getArguments() != null) {
            getArgument(getArguments());
        }
        initView(rootView);
    }

    abstract protected void getArgument(Bundle bundle);

    abstract protected int getLayoutId();

    abstract protected void initView(View root);


    public void replaceFragment(int container, Fragment fragment) {
        replaceFragment(container, fragment, null);
    }

    public void replaceFragment(int container, Fragment fragment, String tag) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        if (tag != null) {
            Fragment f = manager.findFragmentByTag(tag);
            if (f != null) {
                manager.popBackStack();
            }
        }
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        int id = transaction.commit();
    }

    /**
     * This method replace fragment and clear all back stack
     *
     * @param container fragment container
     * @param fragment  new fragment
     */
    public void replaceAndClearFragment(int container, Fragment fragment) {
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE); // clear action
        replaceFragment(container, fragment);
    }

    public void replaceFragmentDontBack(int container, Fragment fragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(container, fragment);
        transaction.commit();
    }

    protected void handleBack() {
        int backStackCnt = getFragmentManager().getBackStackEntryCount();
        if (backStackCnt > 1) {
            getFragmentManager().popBackStack();
        } else {
            getActivity().finish();
        }

    }

    protected void handleBackByTAG(String tag) {
        int backStackCnt = getFragmentManager().getBackStackEntryCount();
        if (backStackCnt > 1) {
            // "0" mean : not pop the fragment with this tag
            getFragmentManager().popBackStack(
                    tag, 0);
        } else {
            getActivity().finish();
        }
    }


}
